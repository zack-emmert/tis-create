# Electric Motor

![No gas, no fuss, just electric motor thrust!](block:createaddition:electric_motor)

The serial port module can also perform I/O with a connected electric motor. A read operation will return the RPM of the motor as a positive integer. A write operation of an integer integer will set the RPM output of the electric motor. Reading or writing an RPM value outside the range [-32768,32767] will result in vendor implementation-specific behavior.
