# Threshold Switch

![Now to switch it up!](block:create:stockpile_switch)

The serial port module is capable of interacting with a Threshold Switch to report the fill level of an attached inventory, or length of an attached rope pulley. A read operation will return the fill level of an inventory as an integer percentage in the range [0,100]. This interface does not support write operations.

If TIS Advanced is installed, the percentage returned will instead be a half-precision float in the range [0.0,1.0].

When reading the length of an attached pulley, the length will be returned as an integer number of blocks.
