package cc.emmert.tiscreate.create;

import java.util.Objects;
import java.util.Optional;

import com.simibubi.create.content.redstone.thresholdSwitch.ThresholdSwitchBlockEntity;

import cc.emmert.tisadvanced.TISAdvanced;
import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class ThresholdSwitchSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.stockpile_switch");
    private static final String DOCUMENTATION_LINK = "threshold_switch.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final ThresholdSwitchBlockEntity stockpileSwitch = Objects.requireNonNull((ThresholdSwitchBlockEntity) level.getBlockEntity(pos));
        return Optional.of(new StockpileSwitchSerialInterface(stockpileSwitch));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof ThresholdSwitchBlockEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof StockpileSwitchSerialInterface;
    }
    
    private class StockpileSwitchSerialInterface implements SerialInterface {

        private ThresholdSwitchBlockEntity stockpileSwitch;

        public StockpileSwitchSerialInterface(ThresholdSwitchBlockEntity s) {
            this.stockpileSwitch = s;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return false;
        }

        @Override
        public short peek() {
            if(ModList.get().isLoaded(TISAdvanced.MOD_ID)) {
                return HalfFloat.toHalf(this.stockpileSwitch.currentLevel);
            } else {
                return (short) Math.round(this.stockpileSwitch.currentLevel * 100);
            }
        }

        @Override
        public void reset() {}

        @Override
        public void skip() {}

        @Override
        public void write(short val) {}

        @Override
        public void load(CompoundTag tag) {}

        @Override
        public void save(CompoundTag tag) {}
    }
}