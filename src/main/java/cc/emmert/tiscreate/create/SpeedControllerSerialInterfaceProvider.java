package cc.emmert.tiscreate.create;

import java.util.Objects;
import java.util.Optional;

import com.simibubi.create.content.kinetics.speedController.SpeedControllerBlockEntity;
import com.simibubi.create.foundation.blockEntity.behaviour.scrollValue.ScrollValueBehaviour;

import cc.emmert.tiscreate.Util;
import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class SpeedControllerSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.rotation_speed_controller");
    private static final String DOCUMENTATION_LINK = "speed_controller.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final SpeedControllerBlockEntity contentObserver = Objects.requireNonNull((SpeedControllerBlockEntity) level.getBlockEntity(pos));
        return Optional.of(new SpeedControllerSerialInterface(contentObserver));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof SpeedControllerBlockEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof SpeedControllerSerialInterface;
    }
    
    private class SpeedControllerSerialInterface implements SerialInterface {

        private SpeedControllerBlockEntity speedController;

        public SpeedControllerSerialInterface(SpeedControllerBlockEntity sc) {
            this.speedController = sc;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return true;
        }

        
        @Override
        public short peek() {
            return (short) Util.clamp(this.getTargetSpeed().getValue());
        }

        @Override
        public void write(short value) {
            this.getTargetSpeed().setValue(value);
        }

        private ScrollValueBehaviour getTargetSpeed() {
            return this.speedController.targetSpeed;
        }

        @Override
        public void reset() {
        }

        @Override
        public void skip() {
        }

        @Override
        public void load(CompoundTag tag) {
        }

        @Override
        public void save(CompoundTag tag) {
        }

    }
}
