package cc.emmert.tiscreate.create;

import java.util.Objects;
import java.util.Optional;

import com.simibubi.create.content.kinetics.gauge.StressGaugeBlockEntity;

import cc.emmert.tiscreate.Util;
import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import li.cil.tis3d.util.EnumUtils;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class StressGaugeSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.stressometer");
    private static final String DOCUMENTATION_LINK = "stress_gauge.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final StressGaugeBlockEntity gauge = Objects.requireNonNull((StressGaugeBlockEntity) level.getBlockEntity(pos));
        return Optional.of(new StressGaugeSerialInterface(gauge));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof StressGaugeBlockEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof StressGaugeSerialInterface;
    }


    private class StressGaugeSerialInterface implements SerialInterface {
        private static final String TAG_BIT_MODE = "bit_mode";
        private static final String TAG_OUTPUT_MODE = "output_mode";

        private enum BITMODE {
            HIGH,
            LOW,
        }

        private enum OUTPUTMODE {
            CURRENT_STRESS,
            STRESS_CAPACITY,
        }

        private StressGaugeBlockEntity gauge;
        private BITMODE bitMode;
        private OUTPUTMODE outputMode;

        public StressGaugeSerialInterface(StressGaugeBlockEntity g) {
            this.gauge = g;
            this.bitMode = BITMODE.LOW;
            this.outputMode = OUTPUTMODE.CURRENT_STRESS;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return true;
        }

        @Override
        public short peek() {
            int output = 0;

            switch(this.outputMode) {
                case CURRENT_STRESS:
                    output = (int) Math.round(gauge.getNetworkStress());
                    break;
                case STRESS_CAPACITY:
                    output = (int) Math.round(gauge.getNetworkCapacity());
                    break;
            }

            switch(this.bitMode) {
                case HIGH:
                    output /= 32_768;
                    break;
                case LOW:
                    output %= 32_768;
                    break;
            }

            return (short) Util.clamp(output);
        }

        @Override
        public void reset() {
            this.bitMode = BITMODE.LOW;
            this.outputMode = OUTPUTMODE.CURRENT_STRESS;
        }

        @Override
        public void skip() {
        }

        @Override
        public void write(short value) {
            if((value & 0x1) == 1) {
                this.bitMode = BITMODE.HIGH;
            } else {
                this.bitMode = BITMODE.LOW;
            }

            if((value & 0x2) == 1) {
                this.outputMode = OUTPUTMODE.STRESS_CAPACITY;
            } else {
                this.outputMode = OUTPUTMODE.CURRENT_STRESS;
            }
        }

        @Override
        public void load(final CompoundTag tag) {
            this.bitMode = EnumUtils.load(StressGaugeSerialInterface.BITMODE.class, TAG_BIT_MODE, tag);
            this.outputMode = EnumUtils.load(StressGaugeSerialInterface.OUTPUTMODE.class, TAG_OUTPUT_MODE, tag);
        }

        @Override
        public void save(final CompoundTag tag) {
            EnumUtils.save(this.bitMode, TAG_BIT_MODE, tag);
            EnumUtils.save(this.outputMode, TAG_OUTPUT_MODE, tag);
        }

    }
}
