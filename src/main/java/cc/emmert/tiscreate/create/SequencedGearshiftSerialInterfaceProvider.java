package cc.emmert.tiscreate.create;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Optional;
import java.util.Vector;

import com.simibubi.create.content.kinetics.transmission.sequencer.Instruction;
import com.simibubi.create.content.kinetics.transmission.sequencer.InstructionSpeedModifiers;
import com.simibubi.create.content.kinetics.transmission.sequencer.SequencerInstructions;
import com.simibubi.create.content.kinetics.transmission.sequencer.SequencedGearshiftBlockEntity;

import li.cil.tis3d.api.machine.HaltAndCatchFireException;
import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import li.cil.tis3d.util.EnumUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class SequencedGearshiftSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.sequenced_gearshift");
    private static final String DOCUMENTATION_LINK = "sequenced_gearshift.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        SequencedGearshiftBlockEntity gearshift = Objects.requireNonNull((SequencedGearshiftBlockEntity) level.getBlockEntity(pos));
        return Optional.of(new SequencedGearshiftSerialInterface(gearshift));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof SequencedGearshiftBlockEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof SequencedGearshiftSerialInterface;
    }

    // Warning: Here there be dragons. The vast majority of the sequenced gearshift data is package-private, and so great amounts of reflection have been done to circumvent it. You have been warned.
    private class SequencedGearshiftSerialInterface implements SerialInterface {

        private enum STATE {
            AWAITING_POSITION,
            POSITION_KNOWN,
        }

        private SequencedGearshiftBlockEntity gearshift;
        private STATE state;
        private int commandRow;
        private int commandCol;

        public SequencedGearshiftSerialInterface(SequencedGearshiftBlockEntity gs) {
            this.gearshift = gs;
            this.state = STATE.AWAITING_POSITION;
            this.commandRow = 0;
            this.commandCol = 0;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return true;
        }

        @Override
        public short peek() {
            Instruction command = this.gearshift.getInstruction(this.commandRow);
            if(command == null)
                throw new HaltAndCatchFireException();

            int commandId = serializeCommandType(command);

            if(this.commandCol == 0) {
                return (short) commandId;
            } else if(this.commandCol == 1 && commandId < 3) {
                return (short) ((int) getInstructionField(command, "value"));
            } else if(this.commandCol == 2 && commandId < 2) {
                return serializeSpeedModifier(command);
            } else {
                throw new HaltAndCatchFireException();
            }
        }

        @Override
        public void write(short val) {
            if(val < 0)
                throw new HaltAndCatchFireException();

            if(this.state == STATE.AWAITING_POSITION) {
                this.commandCol = val & 0b11;
                this.commandRow = (val >> 2) & 0b111;
                this.state = STATE.POSITION_KNOWN;
            } else {
                Instruction command = this.gearshift.getInstruction(this.commandRow);

                // If we're past the end, hard crash
                if(command == null) {
                    throw new HaltAndCatchFireException();
                }
                
                // If we're overwriting the End command, move the end to after it if there's space. The End command is optional if all 5 slots have commands
                if(serializeCommandType(command) == 4 && this.commandRow < this.getInstructions().capacity()) {
                    this.getInstructions().add(new Instruction(SequencerInstructions.END));
                }
 
                if(this.commandCol == 0) {
                    setInstructionField(command, "instruction", deserializeCommandType(val));

                    Vector<Instruction> instructions = this.getInstructions();

                    if(val == 4) {
                        for(int i = instructions.size() - 1; i > this.commandRow; i--) {
                            instructions.remove(i); // Truncate the instruction list if writing an End command
                        }
                    }
                } else if(this.commandCol == 1) {
                    switch((SequencerInstructions) getInstructionField(command, "instruction")) {
                        case TURN_ANGLE:
                            setInstructionField(command, "value", Math.min(val,360));
                            break;
                        case TURN_DISTANCE:
                            setInstructionField(command, "value", Math.min(val,128));
                            break;
                        case DELAY:
                            setInstructionField(command, "value", Math.min(val,600));
                            break;
                        case AWAIT:
                        case END:
                        default:
                        throw new HaltAndCatchFireException();
                    }
                } else if(this.commandCol == 2) {
                    switch((SequencerInstructions) getInstructionField(command, "instruction")) {
                        case TURN_ANGLE:
                            setInstructionField(command, "speedModifier", deserializeSpeedModifier(val));
                            break;
                        case TURN_DISTANCE:
                            setInstructionField(command, "speedModifier", deserializeSpeedModifier(val));
                            break;
                        case DELAY:
                        case AWAIT:
                        case END:
                        default:
                        throw new HaltAndCatchFireException();
                    }
                }

                // Notify the sequenced gearshift that its state has changed so it can send the new instruction list to the client-side UI
                this.gearshift.sendData();
                // Update our own state so we know how to handle the next write
                this.state = STATE.AWAITING_POSITION;
            }
        }

        private static short serializeSpeedModifier(Instruction command) {
            InstructionSpeedModifiers speedModifier = getInstructionField(command, "speedModifier");

                return switch(speedModifier) {
                    case FORWARD -> 0b00;
                    case FORWARD_FAST -> 0b10;
                    case BACK -> 0b01;
                    case BACK_FAST -> 0b11;
                    default -> -1;
                };
        }

        private InstructionSpeedModifiers deserializeSpeedModifier(int val) {

            return switch(val) {
                case 0b00 -> InstructionSpeedModifiers.FORWARD;
                case 0b01 -> InstructionSpeedModifiers.BACK;
                case 0b10 -> InstructionSpeedModifiers.FORWARD_FAST;
                case 0b11 -> InstructionSpeedModifiers.BACK_FAST;
                default -> throw new HaltAndCatchFireException();
            };
        }

        private static short serializeCommandType(Instruction command) {
            SequencerInstructions instructionType = getInstructionField(command, "instruction");

            return switch(instructionType) {
                case AWAIT -> 3;
                case DELAY -> 2;
                case END -> 4;
                case TURN_ANGLE -> 0;
                case TURN_DISTANCE -> 1;
                default -> -1; // Should be unreachable
            };
        }

        private static SequencerInstructions deserializeCommandType(short id) {
            return switch(id) {
                case 0 -> SequencerInstructions.TURN_ANGLE;
                case 1 -> SequencerInstructions.TURN_DISTANCE;
                case 2 -> SequencerInstructions.DELAY;
                case 3 -> SequencerInstructions.AWAIT;
                case 4 -> SequencerInstructions.END;
                default -> throw new HaltAndCatchFireException();
            };
        }

        private static @SuppressWarnings("unchecked") <T> T getInstructionField(Instruction command, String field) {
            try {
                Field instructionField = Instruction.class.getDeclaredField(field);
                instructionField.setAccessible(true);
                return (T) instructionField.get(command);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }

        private static void setInstructionField(Instruction command, String field, Object value) {
            try {
                Field instructionField = Instruction.class.getDeclaredField(field);
                instructionField.setAccessible(true);
                instructionField.set(command,value);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }

        private @SuppressWarnings("unchecked") Vector<Instruction> getInstructions() {
            try {
                Field instructionsField = SequencedGearshiftBlockEntity.class.getDeclaredField("instructions");
                instructionsField.setAccessible(true);
                return (Vector<Instruction>) instructionsField.get(this.gearshift);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void reset() {
            this.state = STATE.AWAITING_POSITION;
            this.commandRow = 0;
            this.commandCol = 0;
        }

        @Override
        public void skip() {
        }

        @Override
        public void load(CompoundTag tag) {
            this.state = EnumUtils.load(STATE.class, "state", tag);
            this.commandRow = tag.getInt("commandRow");
            this.commandCol = tag.getInt("commandCol");
        }

        @Override
        public void save(CompoundTag tag) {
            EnumUtils.save(this.state, "state", tag);
            tag.putInt("commandRow",this.commandRow);
            tag.putInt("commandCol", this.commandCol);
        }
    }
}
