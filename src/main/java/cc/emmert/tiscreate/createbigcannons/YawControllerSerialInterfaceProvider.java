package cc.emmert.tiscreate.createbigcannons;

import java.util.Objects;
import java.util.Optional;

import cc.emmert.tisadvanced.TISAdvanced;
import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.tis3d.api.machine.HaltAndCatchFireException;
import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.registries.ForgeRegistryEntry;
import rbasamoyai.createbigcannons.cannon_control.cannon_mount.CannonMountBlockEntity;
import rbasamoyai.createbigcannons.cannon_control.cannon_mount.YawControllerBlockEntity;

public class YawControllerSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.createbigcannons.yaw_controller");
    private static final String DOCUMENTATION_LINK = "yaw_controller.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final YawControllerBlockEntity yawController = Objects.requireNonNull((YawControllerBlockEntity) level.getBlockEntity(pos));
        return Optional.of(new YawControllerSerialInterface(yawController));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof YawControllerBlockEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof YawControllerSerialInterfaceProvider;
    }
    
    private class YawControllerSerialInterface implements SerialInterface {

        private YawControllerBlockEntity yawController;

        public YawControllerSerialInterface(YawControllerBlockEntity yc) {
            this.yawController = yc;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return false;
        }

        @Override
        public short peek() {
            BlockEntity be = Objects.requireNonNull(yawController.getLevel()).getBlockEntity(yawController.getBlockPos().above());
            if (be instanceof CannonMountBlockEntity cmbe) {
                float yaw = cmbe.getYawOffset(0.0f);

                if(ModList.get().isLoaded(TISAdvanced.MOD_ID)) {
                    return HalfFloat.toHalf(yaw);
                } else {
                    return (short) Math.floor(yaw*10.0f);
                }
            } else {
                throw new HaltAndCatchFireException();
            }
        }

        @Override
        public void reset() {}

        @Override
        public void skip() {}

        @Override
        public void write(short val) {}

        @Override
        public void load(CompoundTag tag) {}

        @Override
        public void save(CompoundTag tag) {}
    }
}
