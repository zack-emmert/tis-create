package cc.emmert.tiscreate.createaddition;

import java.util.Objects;
import java.util.Optional;

import com.mrh0.createaddition.blocks.electric_motor.ElectricMotorBlockEntity;

import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class ElectricMotorSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.createaddition.electric_motor");
    private static final String DOCUMENTATION_LINK = "electric_motor.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final ElectricMotorBlockEntity cannonMount = Objects.requireNonNull((ElectricMotorBlockEntity) level.getBlockEntity(pos));
        return Optional.of(new ElectricMotorSerialInterface(cannonMount));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof ElectricMotorBlockEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof ElectricMotorSerialInterface;
    }

    private class ElectricMotorSerialInterface implements SerialInterface {

        private ElectricMotorBlockEntity motor;

        ElectricMotorSerialInterface(ElectricMotorBlockEntity motor) {
            this.motor = motor;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return true;
        }

        @Override
        public void load(CompoundTag arg0) { }

        @Override
        public short peek() {
            return (short) Math.max(Math.min(Math.round(this.motor.getRPM()),32_767),-32_768);
        }

        @Override
        public void reset() { }

        @Override
        public void save(CompoundTag arg0) { }

        @Override
        public void skip() { }

        @Override
        public void write(short speed) {
            this.motor.setRPM(speed);
        }

    }
    
}
