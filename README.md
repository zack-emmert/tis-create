# Welcome to TIS-Create

TIS-Create is an addon for TIS-3D that provides Serial Port protocols to interface with a variety of blocks from Create and some of its addons. Below is the full list of supported blocks:

**Create**:
- Content Observer
- Rotational Speed Controller
- Sequenced Gearshift
- Speedometer
- Stockpile Switch
- Stressometer

**Create: Crafts and Additions**:
- Modular Accumulator

**Create: Big Cannons**:
- Cannon Mount
- Yaw Controller

The documentation for all implemented serial port protocols can be viewed in-game through the TIS-3D Reference Manual, or online [here](https://gitlab.com/zack-emmert/tis-create/-/tree/main/src/main/resources/assets/tis3d/doc/en_us/protocols).

Support for the below mods is planned, but not yet implemented:

**Create: Simulated family**:
- Navigation Table
- Directional Redstone Link
- Modulating Redstone Link
- Torsion Spring
